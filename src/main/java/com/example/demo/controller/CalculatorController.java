package com.example.demo.controller;

import com.example.demo.service.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.websocket.server.PathParam;

@RestController
public class CalculatorController {
    @Autowired
    CalculatorService calculatorService;

    Logger log = LoggerFactory.getLogger(this.getClass().getName());

    @GetMapping("calc")
    Double calc(@PathParam("a") Double a, @PathParam("b") Double b, @PathParam("operator") Character operator){
        if (a == null || b == null || operator == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "{a, b, operator} required");

        Double res = null;
        try {
            res = calculatorService.calc(a, b, operator);
        }
        catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        log.info("calculated " + a + operator + b + "=" + res);
        return res;
    }
}
