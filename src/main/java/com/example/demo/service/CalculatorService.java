package com.example.demo.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    public Double calc(double num1, double num2, char operator) {

        double answer = 0;

        switch (operator) {
            case '+':
                return num1 + num2;

            case '-':
                return num1 - num2;

            case '*':
                return num1 * num2;

            case '/':
                if (num2 == 0) return null;
                return num1 / num2;
        }
        throw new IllegalArgumentException("invalid operator");
    }

}
