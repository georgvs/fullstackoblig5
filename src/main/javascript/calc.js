const calc = Vue.createApp({
    data(){
        return {
            operator: '',
            prev_num: '0',
            curr_num: '0',
            ans:'0',
            log:''
        }
    },
    methods: {
        clear(){
            this.operator = '';
            this.prev_num = this.curr_num = this.ans = '0';
        },
        set_operator(operator){
            this.operator = operator;
            this.prev_num = this.curr_num;
            this.curr_num = 0;
            console.log(this.operator);
        },
        apnd(num){
            if (num == "." && this.curr_num.includes(".")) return;
            if (this.curr_num == '0') this.curr_num = '';
            this.curr_num += num;
        },
        get(url) {
            return new Promise(function (resolve, reject) {
                let xhr = new XMLHttpRequest();
                xhr.open('GET', url);
                xhr.onload = function () {
                    if (this.status >= 200 && this.status < 300) {
                        resolve(xhr.response);
                    } else {
                        reject({
                            status: this.status,
                            statusText: xhr.statusText
                        });
                    }
                };
                xhr.onerror = function () {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                };
                xhr.send();
            });
        },
        async calc(){
            if (this.operator == '') return;

            n0 = parseFloat(this.prev_num);
            n1 = parseFloat(this.curr_num);

            let res = await this.get('http://localhost:8080/calc?a=' + n0 + '&b=' + n1 + '&operator=' + encodeURIComponent(this.operator));
            if (!res) res = "invalid";

            this.log = n0 + " " + this.operator + " " + n1 + " = " + res + "\n" + this.log;

            this.clear();
            this.ans = "" + res;
            this.curr_num = "" + res;
        },
        ANS(){
            this.curr_num = this.ans;
        },
        DEL(){
            this.curr_num = this.curr_num.substr(0, this.curr_num.length - 1);
            if (this.curr_num == "") this.curr_num = "0";
        }
    }
});

calc.mount("#calc");
